from view import *

### ========================= INITIALISATION ========================= ###

pygame.mixer.init()

pygame.display.set_caption("Etch & Best")

clock = pygame.time.Clock()

#backgroud musique
pygame.mixer.music.load('../music/title-menu.ogg')
pygame.mixer.music.play(loops = -1)
#son

get = pygame.mixer.Sound('../music/truck.wav')
zuwap = pygame.mixer.Sound('../music/ZUWAP.wav')
truck = pygame.mixer.Sound('../music/truck.wav')

#/!\ faut pas changer les noms
sounds = {'get' : get,'zuwap': zuwap, 'truck': truck}
### ========================= FONCTIONS ========================= ###


settings = Settings(sounds)
game = Game(sounds)
high_scores = High_scores()
vue_title_screen = VueTitleScreen(game)
vue_options = VueOptions(game, settings)
vue_credits = VueCredit(game)
vue_jeu = VueJeu(game)
vue_tuto = VueTuto(game)
vue_perdu = VuePerdu(game, high_scores)
vue_victoire = VueVictoire(game,high_scores)
vue_pause = VuePause(game)
vue_keybinds = VueKeybinds(game, settings)
vue_highscore = VueHighScore(game, high_scores)



### ========================= BOUCLE DE JEU ========================= ###

while game.run:
    clock.tick(FPS)
    events = pygame.event.get()

    if game.get_state() == GameState.TITLE_SCREEN:
        # On récupère les touches pressées
        vue_title_screen.handle_input(events, sounds, vue_highscore)
        vue_title_screen.draw()

    elif game.get_state() == GameState.JEU:
        vue_jeu.handle_input(events, settings)
        vue_jeu.tick(events)
        if game.get_state() == GameState.GAGNE:
            vue_victoire.update()
        elif game.get_state() == GameState.PERDU:
            vue_perdu.update()
        vue_jeu.draw()

    elif game.get_state() == GameState.TUTO:
        vue_tuto.handle_events(events, sounds)
        vue_tuto.draw()

    elif game.get_state() == GameState.PERDU:
        vue_perdu.handle_input(events, sounds)
        vue_perdu.draw()

    elif game.get_state() == GameState.GAGNE:
        vue_victoire.handle_input(events, sounds)
        vue_victoire.draw()

    elif game.get_state() == GameState.PAUSE:
        vue_pause.handle_input(events, sounds)
        vue_pause.draw()

    elif game.get_state() == GameState.OPTIONS:
        vue_options.handle_input(events, sounds)
        vue_options.draw(sounds)

    elif game.get_state() == GameState.KEYBINDS:
        vue_keybinds.handle_input(events)
        vue_keybinds.update()
        vue_keybinds.draw()

    elif game.get_state() == GameState.CREDITS:
        vue_credits.handle_input(events, sounds)
        vue_credits.draw()

    elif game.get_state() == GameState.HIGHSCORE:
        vue_highscore.handle_input(events, sounds)
        vue_highscore.draw()


pygame.quit()
