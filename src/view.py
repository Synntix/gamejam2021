from model import *
from settings import *


class VueTitleScreen:

    def __init__(self, game):
        self.__buttons = {"score": Button("High scores", 137, 261, 750),
                          "options": Button("Options", 137 + 20 + 365, 391, (750 - 20) / 2),
                          "credits": Button("Credits", 137, 391, (750 - 20) / 2),
                          "jouer": Button("Jouer", 137, 511, 750),
                          "quitter": Button("Quitter", 137, 631, 750)}

        self.__game = game

    def handle_input(self, events, sounds, highscore):
        for event in events:
            # Si on termine le programme (croix ou alt-f4)
            if event.type == pygame.QUIT:
                self.__game.run = False

            # -- Boutons --
            if event.type == pygame.MOUSEBUTTONDOWN:
                # Position xy de la souris
                if event.button == 1:  # Clic gauche
                    mouse = pygame.mouse.get_pos()
                    # Si on a cliqué sur le bouton jouer
                    if self.__buttons["jouer"].get_rect().collidepoint(mouse):
                        self.__game.set_state(GameState.TUTO)
                        sounds['zuwap'].play()

                    elif self.__buttons["options"].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        self.__game.set_state(GameState.OPTIONS)
                    elif self.__buttons["quitter"].get_rect().collidepoint(mouse):
                        sounds['truck'].play()
                        self.__game.run = False
                    elif self.__buttons["credits"].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        self.__game.set_state(GameState.CREDITS)
                    elif self.__buttons["score"].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        highscore.update()
                        self.__game.set_state(GameState.HIGHSCORE)

    def draw(self):
        background = pygame.image.load(os.path.join('assets', 'background-menu.png'))
        WIN.blit(background, (0, 0))

        for k, button in self.__buttons.items():
            button.draw()

        pygame.display.update()


# ================================================================

class VueTuto:

    def __init__(self, game):
        self.__buttons = {'stage1': Button("Niveau 1", 137, 511, (750 - 3 * 20) / 2 + 20),
                          'stage2': Button("Niveau 2", 137 + (750 - 3 * 20) / 2 + 40, 511, (750 - 3 * 20) / 2 + 20),
                          'retour': Button("Retour au menu", 137, 631, 750)}

        self.__game = game

    def handle_events(self, events, sounds):
        for event in events:

            # Si on termine le programme (croix ou alt-f4)
            if event.type == pygame.QUIT:
                self.__game.set_state(GameState.TITLE_SCREEN)
                self.__game.run = False

            # -- Boutons --
            if event.type == pygame.MOUSEBUTTONDOWN:
                # Position xy de la souris
                if event.button == 1:  # Clic gauche
                    mouse = pygame.mouse.get_pos()

                    # Si on a cliqué sur le 1er niveau
                    if self.__buttons['stage1'].get_rect().collidepoint(mouse):
                        # Définir l'état en mode jeu
                        self.__game.set_state(GameState.JEU)
                        self.__game.reset(1)
                        self.__game.track_number = 1

                    # Si on a cliqué sur le 2e niveau
                    if self.__buttons['stage2'].get_rect().collidepoint(mouse):
                        # Définir l'état en mode jeu
                        self.__game.set_state(GameState.JEU)
                        self.__game.reset(2)
                        self.__game.track_number = 2

                    # Si on a cliqué sur Retour
                    elif self.__buttons['retour'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        self.__game.set_state(GameState.TITLE_SCREEN)

    def draw(self):
        WIN.fill(WHITE)
        x_txt = 50
        y_txt = 30
        text = FONT.render("Comment jouer :", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (x_txt, y_txt))
        # + 45 En plus pour le texte
        y_txt += 45
        text = FONT_TXT.render("Le camion roule sur une route à 3 voies pleine de fruits et légumes.", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (x_txt, y_txt))
        y_txt += 45
        text = FONT_TXT.render("Parmi les deux personnages, (Etch le conducteur et Best le cuisinier), un seul est", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (x_txt, y_txt))
        y_txt += 45
        text = FONT_TXT.render("éveillé et perd progressivement de l'énergie, pendant que l'autre dort et la récupère.", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (x_txt, y_txt))
        y_txt += 45
        text = FONT_TXT.render("- Quand le chauffeur est réveillé, il doit esquiver les végétaux géants", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (x_txt, y_txt))
        y_txt += 45
        text = FONT_TXT.render("en pilotant le camion avec [ZQSD].", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (x_txt, y_txt))
        y_txt += 45
        text = FONT_TXT.render("- Quand le cuistot est réveillé, il doit trancher les fruits et légumes", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (x_txt, y_txt))
        y_txt += 45
        text = FONT_TXT.render("devant le camion avec [QSD].", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (x_txt, y_txt))
        y_txt += 45
        text = FONT_TXT.render("Alternez habilement les personnages avec [Shift] pour garder l'équilibre des énergies", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (x_txt, y_txt))
        y_txt += 45
        text = FONT_TXT.render("et engranger un maximum de points !", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (x_txt, y_txt))

        for k, button in self.__buttons.items():
            button.draw()

        pygame.display.update()


class VueJeu:

    def __init__(self, game):
        self.__buttons = {}
        self.__game = game
        self.__landscape = Landscape("backgroundTest.png")

    def handle_input(self, events, settings):
        for event in events:
            # Si on termine le programme (croix ou alt-f4)
            if event.type == pygame.QUIT:
                self.__game.set_state(GameState.TITLE_SCREEN)
                self.__game.run = False

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.__game.set_state(GameState.PAUSE)
                    pygame.mixer.music.pause()
                elif event.key == pygame.K_F3:
                    self.__game.toggle_show_hitboxes()
                elif event.key == pygame.K_q or event.key == pygame.K_s or event.key == pygame.K_d:
                    self.__game.car.animate_cooker()

        pressed = pygame.key.get_pressed()

        if pressed[pygame.key.key_code(settings.get_keybinds()['mode'])]:
            clock = perf_counter()
            if clock - self.__game.get_last_switch() > 0.75:
                self.__game.set_last_switch(clock)
                self.__game.driver.switch_state()
                self.__game.slicer.switch_state()
                self.__game.car.tp(self.__game.driver.isActive())

        if self.__game.driver.isActive():
            if pressed[pygame.key.key_code(settings.get_keybinds()['haut'])] or pressed[pygame.K_UP]:
                self.__game.car.move_y(-1)

            if pressed[pygame.key.key_code(settings.get_keybinds()['bas'])] or pressed[pygame.K_DOWN]:
                self.__game.car.move_y(1)

            if pressed[pygame.key.key_code(settings.get_keybinds()['gauche'])] or pressed[pygame.K_LEFT]:
                self.__game.car.move_x(-1)

            if pressed[pygame.key.key_code(settings.get_keybinds()['droite'])] or pressed[pygame.K_RIGHT]:
                self.__game.car.move_x(1)
            else:
                self.__game.car.move_x(-1 / 4)

    def draw(self):
        debug = self.__game.get_show_hitboxes()
        self.__landscape.draw()
        self.__game.road.draw(debug)
        self.__game.car.draw(debug, self.__game.driver.isActive())
        if self.__game.slicer.isActive():
            self.__game.car.draw_slicing_bar()
        self.__game.driver.drawStamina()
        self.__game.slicer.drawStamina()
        self.__game.car.drawHealthBar()
        self.__game.score.drawScore()
        self.__game.score.drawCombo()
        self.__game.driverPic.draw()
        self.__game.slicerPic.draw()
        pygame.display.update()

    def tick(self, events):
        if self.__game.score.getCurrentCombo() == 5:
            self.__game.car.change_hp(10)

            self.__game.score.clearCombo()

        if self.__game.driver.isActive():
            self.__game.driver.changeStamina(-1 / 2)
            self.__game.slicer.changeStamina(1 / 2)
        else:
            self.__game.slicer.changeStamina(-1 / 2)
            self.__game.driver.changeStamina(1 / 2)

        if self.__game.driver.getStamina() == 0:
            self.__game.car.change_hp(-1 / 16)

        if self.__game.slicer.getStamina() == 0:
            self.__game.car.change_hp(-1 / 32)

        self.__game.road.incr_time(6)
        self.__landscape.incr_time(1 / 3)
        self.__game.road.tick()

        visible_items = self.__game.road.get_visible_items()
        if self.__game.driver.isActive():
            for item in visible_items:
                if not (item.is_sliced()):
                    if self.__game.car.get_close_rect().colliderect(item.get_rect()):
                        self.__game.score.addScore(5)
                    if self.__game.car.get_rect().colliderect(item.get_rect()):
                        self.__game.car.change_hp(-1.5)
                        item.slice()

        else:
            for item in visible_items:
                if (
                        not item.is_missed() and item.get_rect().x + item.get_rect().width < self.__game.car.get_slicing_rect().x):
                    # self.__game.score.clearCombo()
                    # item.miss()
                    # self.__game.car.change_hp(-1)
                    pass

            sliced = False
            for event in events:

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        for item in visible_items:

                            if self.__game.car.get_slicing_rect(0).colliderect(item.get_rect()):
                                item.slice()
                                sliced = True
                                self.__game.car.change_hp(1)
                                points = Game.calcul_points(
                                    self.__game.car.get_slicing_rect().x + self.__game.car.get_slicing_rect().width / 2,
                                    item.get_rect().x + item.get_rect().width / 2)
                                self.__game.score.addScore(points)
                                self.__game.score.addCombo()

                    if event.key == pygame.K_s:
                        for item in visible_items:

                            if self.__game.car.get_slicing_rect(1).colliderect(item.get_rect()):
                                item.slice()
                                sliced = True
                                self.__game.car.change_hp(1)
                                points = Game.calcul_points(
                                    self.__game.car.get_slicing_rect().x + self.__game.car.get_slicing_rect().width / 2,
                                    item.get_rect().x + item.get_rect().width / 2)
                                self.__game.score.addScore(points)
                                self.__game.score.addCombo()

                    if event.key == pygame.K_d:
                        for item in visible_items:
                            if self.__game.car.get_slicing_rect(2).colliderect(item.get_rect()):
                                item.slice()
                                sliced = True
                                self.__game.car.change_hp(1)
                                points = Game.calcul_points(
                                    self.__game.car.get_slicing_rect().x + self.__game.car.get_slicing_rect().width / 2,
                                    item.get_rect().x + item.get_rect().width / 2)
                                self.__game.score.addScore(points)
                                self.__game.score.addCombo()

                    if not sliced:
                        self.__game.score.clearCombo()

        if self.__game.car.get_hp() <= 0.0:
            self.__game.set_state(GameState.PERDU)
            pygame.mixer.music.pause()


class VuePerdu:

    def __init__(self, game, high_scores):
        self.__game = game
        self.__strings = {'perdu_str': FONT.render("Vous avez perdu !", 1, pygame.Color("#7D7D7D")),
                          'score_str': FONT.render("Score : {}".format(self.__game.score.get_score()), 1,
                                                   pygame.Color("#7D7D7D")),
                          'input_pseudo_str': FONT.render("Pseudo :", 1, pygame.Color("#7D7D7D"))}

        self.__strings_coords = {'perdu_str': (WIDTH / 2 - self.__strings['perdu_str'].get_width() / 2, 100),
                                 'score_str': (WIDTH / 2 - self.__strings['score_str'].get_width() / 2, 2 * 50 + 100),
                                 'input_pseudo_str': (WIDTH / 2 - self.__strings['input_pseudo_str'].get_width() - 20,
                                                      4 * 50 + 125 - self.__strings[
                                                          'input_pseudo_str'].get_height() / 2)}

        self.__buttons = {'input_pseudo': InputBox(WIDTH / 2 + 50, 4 * 50 + 125 - 50 / 2, 200, 50),
                          'reessayer': Button("Reessayer", WIDTH / 8, HEIGHT / 2 + 50, WIDTH / 1.3),
                          'abandonner': Button("Abandonner", WIDTH / 8, HEIGHT / 2 + 2 * 50 + 100, WIDTH / 1.3)}
        self.__high_scores = high_scores

    def handle_input(self, events, sounds):
        for event in events:
            # Si on termine le programme (croix ou alt-f4)
            if event.type == pygame.QUIT:
                self.__game.set_state(GameState.TITLE_SCREEN)
                self.__game.run = False

            self.__buttons['input_pseudo'].handle_event(event)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:  # Clic gauche
                    # Position xy de la souris
                    mouse = pygame.mouse.get_pos()
                    # Si on a cliqué sur le bouton réessayer
                    if self.__buttons['reessayer'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        # high_scores.add_score("piste{}".format(road.get_num()), input_pseudo.text, score.get_score())
                        self.__high_scores.add_score('piste1', self.__buttons['input_pseudo'].text.lower(),
                                                     self.__game.score.get_score())
                        self.__high_scores.save()
                        self.__game.reset(self.__game.track_number)
                        self.__game.set_state(GameState.JEU)
                    # Si on a cliqué sur le bouton abandonner
                    elif self.__buttons['abandonner'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        self.__high_scores.add_score('piste1', self.__buttons['input_pseudo'].text.lower(),
                                                     self.__game.score.get_score())
                        self.__high_scores.save()
                        self.__game.set_state(GameState.TITLE_SCREEN)

    def update(self):
        self.__strings['score_str'] = FONT.render("Score : {}".format(self.__game.score.get_score()), 1,
                                                   pygame.Color("#7D7D7D"))

    def draw(self):
        WIN.fill(WHITE)

        for k, v in self.__strings.items():
            WIN.blit(v, self.__strings_coords[k])

        self.__buttons['input_pseudo'].update()

        for k, v in self.__buttons.items():
            v.draw()

        pygame.display.update()


class VueVictoire:

    def __init__(self, game, high_scores):
        self.__game = game
        self.__strings = {'victoire_str': FONT.render("On dirait Brian May !", 1, pygame.Color("#7D7D7D")),
                          'score_str': FONT.render("Score : {}".format(self.__game.score.get_score()), 1,
                                                   pygame.Color("#7D7D7D")),
                          'input_pseudo_str': FONT.render("Pseudo :", 1, pygame.Color("#7D7D7D"))}

        self.__strings_coords = {'victoire_str': (WIDTH / 2 - self.__strings['victoire_str'].get_width() / 2, 100),
                                 'score_str': (WIDTH / 2 - self.__strings['score_str'].get_width() / 2, 2 * 50 + 100),
                                 'input_pseudo_str': (WIDTH / 2 - self.__strings['input_pseudo_str'].get_width() - 20,
                                                      4 * 50 + 125 - self.__strings[
                                                          'input_pseudo_str'].get_height() / 2)}

        self.__buttons = {'input_pseudo': InputBox(WIDTH / 2 + 50, 4 * 50 + 125 - 50 / 2, 200, 50),
                          'Choix_niveau': Button("Choix du niveau", WIDTH / 8, HEIGHT / 2 + 2 * 50 + 100, WIDTH / 1.3)}
        self.__high_scores = high_scores



    def handle_input(self, events, sounds):
        for event in events:
            # Si on termine le programme (croix ou alt-f4)
            if event.type == pygame.QUIT:
                self.__game.set_state(GameState.TITLE_SCREEN)
                self.__game.run = False

            self.__buttons['input_pseudo'].handle_event(event)

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:  # Clic gauche
                    # Position xy de la souris
                    mouse = pygame.mouse.get_pos()
                    # Si on a cliqué sur le bouton réessayer
                    if self.__buttons['Choix_niveau'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        # high_scores.add_score("piste{}".format(road.get_num()), input_pseudo.text, score.get_score())
                        self.__high_scores.add_score('piste1', self.__buttons['input_pseudo'].text.lower(),
                                                     self.__game.score.get_score())
                        self.__high_scores.save()
                        self.__game.set_state(GameState.TITLE_SCREEN)

    def update(self):
        def update(self):
            self.__strings['score_str'] = FONT.render("Score : {}".format(self.__game.score.get_score()), 1,
                                                      pygame.Color("#7D7D7D"))

    def draw(self):
        WIN.fill(WHITE)

        for k, v in self.__strings.items():
            WIN.blit(v, self.__strings_coords[k])

        self.__buttons['input_pseudo'].update()

        for k, v in self.__buttons.items():
            v.draw()

        pygame.display.update()



class VuePause:

    def __init__(self, game):
        self.__game = game
        self.__buttons = {'cancel': Button("Retour au menu", 137, 531, 750),
                          'reprendre': Button("Reprendre le jeu", 137, 381, 750)}

        self.__strings = {'pause': FONT.render("Pause", 1, pygame.Color("#7D7D7D"))}
        self.__strings_coords = {'pause': (WIDTH / 2 - self.__strings['pause'].get_width() / 2, HEIGHT / 4)}

    def handle_input(self, events, sounds):
        for event in events:
            # Si on termine le programme (croix ou alt-f4)
            if event.type == pygame.QUIT:
                self.__game.set_state(GameState.TITLE_SCREEN)
                self.__game.run = False

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    sounds['zuwap'].play()
                    self.__game.set_state(GameState.JEU)
                    pygame.mixer.music.unpause()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:  # Clic gauche
                    # Position xy de la souris
                    mouse = pygame.mouse.get_pos()
                    # Si on a cliqué sur le bouton reprendre
                    if self.__buttons['reprendre'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        self.__game.set_state(GameState.JEU)
                        pygame.mixer.music.unpause()
                        self.__game.road.initial_tick()
                    # Si on a cliqué sur le bouton retour au menu
                    elif self.__buttons['cancel'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        pygame.mixer.music.pause()
                        self.__game.set_state(GameState.TITLE_SCREEN)

    def draw(self):
        WIN.fill(WHITE)

        for k, v in self.__strings.items():
            WIN.blit(v, self.__strings_coords[k])

        for k, v in self.__buttons.items():
            v.draw()

        pygame.display.update()


class VueOptions:

    def __init__(self, game, settings):
        self.__game = game
        self.__settings = settings

        self.__buttons = {'controle': Button("Controles", 137, 511, 750),
                          'musicUp': Button("+", 137 + 3 * ((750 + 20) / 4), 391, (750 - 3 * 20) / 4),
                          'musicDown': Button("-", 137 + 2 * ((750 + 20) / 4), 391, (750 - 3 * 20) / 4),
                          'fxUp': Button("+", 137 + 3 * ((750 + 20) / 4), 271, (750 - 3 * 20) / 4),
                          'fxDown': Button("-", 137 + 2 * ((750 + 20) / 4), 271, (750 - 3 * 20) / 4),
                          'retour': Button("Retour au menu", 137, 631, 750)}

    def handle_input(self, events, sounds):
        for event in events:
            # Si on termine le programme (croix ou alt-f4)
            if event.type == pygame.QUIT:
                self.__game.set_state(GameState.TITLE_SCREEN)
                self.__game.run = False

            # -- Boutons --
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:  # Clic gauche
                    # Position xy de la souris
                    mouse = pygame.mouse.get_pos()
                    # Si on a cliqué sur le bouton options
                    if self.__buttons['retour'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        self.__settings.save()
                        self.__game.set_state(GameState.TITLE_SCREEN)
                    elif self.__buttons['musicUp'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        pygame.mixer.music.set_volume(round(pygame.mixer.music.get_volume(), 1) + 0.1)
                    elif self.__buttons['musicDown'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        pygame.mixer.music.set_volume(round(pygame.mixer.music.get_volume(), 1) - 0.1)

                    elif self.__buttons['fxUp'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        for key, sound in sounds.items():
                            sound.set_volume(round(sound.get_volume(), 1) + 0.1)
                    elif self.__buttons['fxDown'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        for key, sound in sounds.items():
                            sound.set_volume(round(sound.get_volume(), 1) - 0.1)

                    elif self.__buttons['controle'].get_rect().collidepoint(mouse):
                        self.__game.set_state(GameState.KEYBINDS)

    def draw(self, sounds):
        WIN.fill(WHITE)

        for k, v in self.__buttons.items():
            v.draw()

        fx = FONT.render("Effets : {}".format(int(round(sounds['get'].get_volume(), 1) * 10)), 1,
                         pygame.Color("#7D7D7D"))
        WIN.blit(fx, (137 + 0 * ((750 + 20) / 4), 291))

        music = FONT.render("Musique : {}".format(int(round(pygame.mixer.music.get_volume(), 1) * 10)), 1,
                            pygame.Color("#7D7D7D"))
        WIN.blit(music, (137 + 0 * ((750 + 20) / 4), 411))

        pygame.display.update()


class VueKeybinds:

    def __init__(self, game, settings):
        self.__game = game
        self.__settings = settings
        self.margin = 20
        self.margin_top = 30

        self.return_button = Button("Retour aux options", WIDTH / 8, HEIGHT - self.margin_top - 100, WIDTH / 1.3)

        self.buttons_lbl = []
        go_top_lbl = FONT.render("Aller a en haut:", 1, pygame.Color("#7D7D7D"))
        go_bottom_lbl = FONT.render("Aller en bas:", 1, pygame.Color("#7D7D7D"))
        go_left_lbl = FONT.render("Aller a gauche:", 1, pygame.Color("#7D7D7D"))
        go_right_lbl = FONT.render("Aller a droite:", 1, pygame.Color("#7D7D7D"))
        switch_mode_lbl = FONT.render("Changer de personnage:", 1, pygame.Color("#7D7D7D"))
        self.buttons_lbl.append(go_top_lbl)
        self.buttons_lbl.append(go_bottom_lbl)
        self.buttons_lbl.append(go_left_lbl)
        self.buttons_lbl.append(go_right_lbl)
        self.buttons_lbl.append(switch_mode_lbl)

        self.buttons_lbl_coords = []
        go_top_lbl_coords = (WIDTH / 1.2 - self.margin - go_top_lbl.get_width(), self.margin + self.margin_top)
        go_bottom_lbl_coords = (
        WIDTH / 1.2 - self.margin - go_bottom_lbl.get_width(), 2 * self.margin + 100 + self.margin_top)
        go_left_lbl_coords = (
        WIDTH / 1.2 - self.margin - go_left_lbl.get_width(), 3 * self.margin + 2 * 100 + self.margin_top)
        go_right_lbl_coords = (
        WIDTH / 1.2 - self.margin - go_right_lbl.get_width(), 4 * self.margin + 3 * 100 + self.margin_top)
        switch_mode_lbl_coords = (
        WIDTH / 1.2 - self.margin - switch_mode_lbl.get_width(), 5 * self.margin + 4 * 100 + self.margin_top)
        self.buttons_lbl_coords.append(go_top_lbl_coords)
        self.buttons_lbl_coords.append(go_bottom_lbl_coords)
        self.buttons_lbl_coords.append(go_left_lbl_coords)
        self.buttons_lbl_coords.append(go_right_lbl_coords)
        self.buttons_lbl_coords.append(switch_mode_lbl_coords)

        self.keybinds_label_list = []
        for i in range(len(self.buttons_lbl)):
            self.keybinds_label_list.append((self.buttons_lbl[i], self.buttons_lbl_coords[i]))

        self.update()

    def update(self):
        go_top_str = self.__settings.get_keybinds()['haut']
        go_bottom_str = self.__settings.get_keybinds()['bas']
        go_left_str = self.__settings.get_keybinds()['gauche']
        go_right_str = self.__settings.get_keybinds()['droite']
        switch_mode_str = self.__settings.get_keybinds()['mode']

        go_top_btn = Button(go_top_str, WIDTH / 1.2 + self.margin, self.buttons_lbl_coords[0][1] - 30)
        go_bottom_btn = Button(go_bottom_str, WIDTH / 1.2 + self.margin, self.buttons_lbl_coords[1][1] - 30)
        go_left_btn = Button(go_left_str, WIDTH / 1.2 + self.margin, self.buttons_lbl_coords[2][1] - 30)
        go_right_btn = Button(go_right_str, WIDTH / 1.2 + self.margin, self.buttons_lbl_coords[3][1] - 30)
        switch_mode_btn = Button(switch_mode_str, WIDTH / 1.2 + self.margin, self.buttons_lbl_coords[4][1] - 30)

        self.keybinds_button_list = []
        self.keybinds_button_list.append(go_top_btn)
        self.keybinds_button_list.append(go_bottom_btn)
        self.keybinds_button_list.append(go_left_btn)
        self.keybinds_button_list.append(go_right_btn)
        self.keybinds_button_list.append(switch_mode_btn)

    def assign_keybind(self, action):
        not_checked = 1
        while not_checked:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    self.__settings.get_keybinds()[action] = pygame.key.name(event.key)
                    self.update()
                    self.__settings.save()
                    not_checked = 0

    def handle_input(self, events):
        for event in events:
            # Si on termine le programme (croix ou alt-f4)
            if event.type == pygame.QUIT:
                self.__game.set_state(GameState.TITLE_SCREEN)
                self.__game.run = False

            if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:  # Clic gauche
                # Position xy de la souris
                mouse = pygame.mouse.get_pos()

                if self.keybinds_button_list[0].get_rect().collidepoint(mouse):
                    self.assign_keybind('haut')
                elif self.keybinds_button_list[1].get_rect().collidepoint(mouse):
                    self.assign_keybind('bas')
                elif self.keybinds_button_list[2].get_rect().collidepoint(mouse):
                    self.assign_keybind('gauche')
                elif self.keybinds_button_list[3].get_rect().collidepoint(mouse):
                    self.assign_keybind('droite')
                elif self.keybinds_button_list[4].get_rect().collidepoint(mouse):
                    self.assign_keybind('mode')
                elif self.return_button.get_rect().collidepoint(mouse):
                    self.__game.set_state(GameState.OPTIONS)

    def draw(self):
        WIN.fill(WHITE)

        for button in self.keybinds_button_list:
            button.draw()

        for (label, coords) in self.keybinds_label_list:
            WIN.blit(label, coords)

        self.return_button.draw()

        pygame.display.update()


class VueCredit:

    def __init__(self, game):
        self.__game = game

        self.__buttons = {'retour': Button("Retour au menu", 137, 631, 750)}

    def handle_input(self, events, sounds):
        for event in events:
            #Si on termine le programme (croix ou alt-f4)
            if event.type == pygame.QUIT:
                self.__game.set_state(GameState.TITLE_SCREEN)
                self.__game.run = False

            # -- Boutons --
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1: #Clic gauche
                    #Position xy de la souris
                    mouse = pygame.mouse.get_pos()
                    #Si on a cliqué sur le bouton options
                    if self.__buttons['retour'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        self.__game.set_state(GameState.TITLE_SCREEN)
                        # On dessine la fenetre de credit

    def draw(self):
        WIN.fill(WHITE)

        text = FONT.render("Credits (non exhaustifs) :", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (110, 80))

        text = FONT_TXT.render("Les Con-cepteurs : Alexandre, Elian, Eliott et Theo", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (137, 137))
        text = FONT_TXT.render("Musiques et Fx: ", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (137, 172))
        text = FONT_TXT.render("   Ecran titre : Pressure Cooker - Jeremy Korpas", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (137, 207))
        text = FONT_TXT.render("   Niveau 1 : We're Not Gonna Take It - Twisted Sister", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (137, 242))
        text = FONT_TXT.render("   Niveau 2 : Hells Bells - AC/DC", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (137, 277))
        text = FONT_TXT.render("   Bouton menu : Hakaishiin", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (137, 312))

        text = FONT_TXT.render("Graphisme : Delphine et MEL", 1, pygame.Color("#7D7D7D"))
        WIN.blit(text, (137, 347))

        for k, v in self.__buttons.items():
            v.draw()

        pygame.display.update()


class VueHighScore:

    def __init__(self, game, high_score_data):
        self.__game = game
        self.__high_score_data = high_score_data
        self.__buttons = {'retour': Button("Retour au menu", 137, 631, 750)}


        outer_margin = 50
        inner_margin = 10
        self.__main_rect = pygame.Rect(outer_margin, outer_margin, WIDTH - 2 * outer_margin,
                                       HEIGHT - 2 * outer_margin)
        width = self.__main_rect.width/2
        height = self.__main_rect.height/2
        self.__rects = []
        self.__rects.append(pygame.Rect(self.__main_rect.x, self.__main_rect.y, width, height))
        self.__rects.append(pygame.Rect(self.__main_rect.x + width + inner_margin, self.__main_rect.y, width, height))
        self.__rects.append(pygame.Rect(self.__main_rect.x, self.__main_rect.y + height + inner_margin, width, height))
        self.__rects.append(
            pygame.Rect(self.__main_rect.x + width + inner_margin, self.__main_rect.y + height + inner_margin, width,
                        height))

        font_title = Font(os.path.join('assets', '04B_30__.ttf'), 25)
        self.__title_strings = []
        self.__title_strings_pos = []
        for i in range(4):
            self.__title_strings.append(font_title.render("Piste {}".format(i), 1, pygame.Color("#7D7D7D")))
            self.__title_strings_pos.append((self.__rects[i].x + width/2 - self.__title_strings[i].get_width(),
                                        self.__rects[i].y + 10))

        self.__classement_strings = [[]]
        self.__classement_strings_pos = []
        for i in range(4):
            self.__classement_strings.append([])
            for j in range(3, 0, -1):
                score = high_score_data.get_score(i+1, j)
                self.__classement_strings[i].append(font_title.render("{}   {}".format(score[0], score[1]), 1, pygame.Color("#7D7D7D")))

        for i in range(4):
            self.__classement_strings_pos.append([])
            for j in range(2, -1, -1):
                self.__classement_strings_pos[i].append(
                    (self.__rects[i].x + 20,
                     self.__rects[i].y + 100 + j * 30))

    def draw(self):
        WIN.fill(WHITE)

        for i in range(4):
            WIN.blit(self.__title_strings[i], self.__title_strings_pos[i])
            for j in range(3):
                WIN.blit(self.__classement_strings[i][j], self.__classement_strings_pos[i][j])

        for k, v in self.__buttons.items():
            v.draw()

        pygame.display.update()

    def handle_input(self, events, sounds):
        for event in events:
            #Si on termine le programme (croix ou alt-f4)
            if event.type == pygame.QUIT:
                self.__game.set_state(GameState.TITLE_SCREEN)
                self.__game.run = False

            # -- Boutons --
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1: #Clic gauche
                    #Position xy de la souris
                    mouse = pygame.mouse.get_pos()
                    #Si on a cliqué sur le bouton retour
                    if self.__buttons['retour'].get_rect().collidepoint(mouse):
                        sounds['zuwap'].play()
                        self.__game.set_state(GameState.TITLE_SCREEN)
                        # On dessine la fenetre de highscore


    def update(self):
        font_title = Font(os.path.join('assets', '04B_30__.ttf'), 25)

        self.__classement_strings = [[]]
        for i in range(4):
            self.__classement_strings.append([])
            for j in range(3, 0, -1):
                score = self.__high_score_data.get_score(i+1, j)
                self.__classement_strings[i].append(font_title.render("{}   {}".format(score[0], score[1]), 1, pygame.Color("#7D7D7D")))
