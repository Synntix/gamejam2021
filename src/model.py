from enum import Enum
import pygame
import yaml
import os
from math import ceil, floor
from time import perf_counter
from random import choice
from settings import *

### ========================= CAR ========================= ###

class Car:

    __x = None
    __y = None
    __pv = None
    __width = None
    __height = None
    __sprite = None
    __velocity = None

    def __init__(self, x=200, y=485, width=227, height=100, hp=20, velocity=1, sprite="foodtruck", rectRoad=None, rectClose=None):
        self.__x = x
        self.__y = y
        self.__hp = hp
        self.__MINHP = 0
        self.__MAXHP = 30
        self.__width = width
        self.__height = height
        self.__sprites = []
        for i in range(3):
            self.__sprites.append(pygame.transform.scale(pygame.image.load(os.path.join('assets', "{}{}.png".format(sprite, i))), (250, 150)))

        self.__cooker_sprites = []
        for i in range(3):
            self.__cooker_sprites.append(pygame.transform.scale(pygame.image.load(os.path.join('assets', "{}{}.png".format('cook', i))), (250, 150)))

        self.__driver_sprites = []
        for i in range(2):
            self.__driver_sprites.append(
                pygame.transform.scale(pygame.image.load(os.path.join('assets', "{}{}.png".format('driver', i))),
                                       (250, 150)))

        self.__cooker_slice_animation_cd = 0
        self.__animation_timing = 0
        # Nombre de secondes que reste une frame d'animation
        self.__amination_fps = 4

        self.__rect = pygame.Rect(self.__x, self.__y, self.__width, self.__height)
        self.__rectRoad = rectRoad
        self.__rectClose = pygame.Rect(self.__x-50, self.__y-50, self.__width+100, self.__height+100)

        self.__slicing_pos = []
        self.__slicing_pos.append((self.__width/-2, HEIGHT-self.__rectRoad.height + 30 + self.__height/3/2 - self.__height/2))
        self.__slicing_pos.append((self.__width/-2, self.__rectRoad.y + self.__rectRoad.height/2 - height/2))
        self.__slicing_pos.append((self.__width/-2, HEIGHT - 30 - self.__rectRoad.height/3/2 - self.__height/2))

        self.__slicing_rects = []
        self.__slicing_rects.append(pygame.Rect(WIDTH/8+25, self.__rectRoad.y, 5, self.__rectRoad.height/3-50))
        self.__slicing_rects.append(pygame.Rect(WIDTH/8+25, self.__rectRoad.y*(1+2/3) + 20, 5, self.__rectRoad.height/3-50))
        self.__slicing_rects.append(pygame.Rect(WIDTH/8+25, self.__rectRoad.y*(1+4/3) + 25, 5, self.__rectRoad.height/3))
        # # SLICING BAR VERSION RECTANGLE REMPLI
        # self.__slicing_surface = pygame.Surface((5, self.__rectRoad.height))
        # self.__slicing_surface.fill(BLACK)
        # SLICING BAR VERSION IMAGE
        self.__slicing_surface = pygame.image.load(os.path.join('assets', 'bar.png'))
        self.__velocity = velocity

        self.__healthBar = Bar(WIDTH-310, 20, 300, 22.5, "#FF0000", True)
        self.__healthBarFrame = Box(x=WIDTH-365, y=7, width=470, height=50, fileName="barre_vie.png")
        self.__healthBarFrame.scale(width=367, height=50)

        # self.__staminaBar = Bar(x=160, y=bar_y, width=stamina, height=22.5, color=bar_color)
        # self.__staminaBarFrame = Box(x=90, y=bar_y-13, width=stamina, height=50, fileName=filename)

    def draw(self, debug, driver_active):
        self.__animation_timing += 1
        mod = self.__animation_timing % (3*self.__amination_fps)
        if mod < (self.__amination_fps):
            WIN.blit(self.__sprites[0], (self.__x-20, self.__y-50))
        elif (self.__amination_fps) <= mod < (3*self.__amination_fps * (2/3)):
            WIN.blit(self.__sprites[1], (self.__x-20, self.__y-50))
        else:
            WIN.blit(self.__sprites[2], (self.__x-20, self.__y-50))

        if self.__cooker_slice_animation_cd > 0:
            self.__cooker_slice_animation_cd -= 1

        if driver_active:
            WIN.blit(self.__cooker_sprites[0], (self.__x-20, self.__y-50))
            WIN.blit(self.__driver_sprites[1], (self.__x-20, self.__y-50))
        else:
            WIN.blit(self.__driver_sprites[0], (self.__x-20, self.__y-50))
            if self.__cooker_slice_animation_cd > 0:
                WIN.blit(self.__cooker_sprites[2], (self.__x - 20, self.__y - 50))
            else:
                WIN.blit(self.__cooker_sprites[1], (self.__x - 20, self.__y - 50))


        #Hitbox debug
        if (debug):
            pygame.draw.rect(WIN, WHITE, self.__rect, 5)

            for i in self.__slicing_rects:
                pygame.draw.rect(WIN, BLACK, i, 1)
            #Hitbox debug pour rectClose
            # s = pygame.Surface((self.__rectClose.width, self.__rectClose.height))
            # s.fill(BLACK)
            # WIN.blit(s, (self.__rectClose.x, self.__rectClose.y))

    def animate_cooker(self):
        self.__cooker_slice_animation_cd = 3

    def draw_slicing_bar(self):
        # WIN.blit(self.__slicing_surface, (WIDTH/8+25, self.__rectRoad.y))
        WIN.blit(self.__slicing_surface, (WIDTH/8+23, self.__rectRoad.y+55))

    def drawHealthBar(self):
        self.__healthBar.draw()
        self.__healthBarFrame.draw()


    def move_x(self, x):
        if 0 < self.__rect.x+self.__velocity*x < WIDTH-self.__rect.width:
            self.__x += self.__velocity*x
            self.__rect.x = self.__x
            self.__rectClose.x =  self.__x - 50
    def move_y(self, y):
        if self.__rectRoad.y < self.__rect.y+self.__velocity*y < HEIGHT-self.__rect.height:
            self.__y += self.__velocity*y
            self.__rect.y = self.__y
            self.__rectClose.y=  self.__y - 50

    def get_hp(self):
        return self.__hp

    def get_slicing_rect(self, i=0):
        return self.__slicing_rects[i]

    def change_hp(self, d=5):
        if self.__MINHP <= self.__hp <= self.__MAXHP:
            self.__hp += d
            self.__healthBar.change_width(d*10)
            self.__healthBar.changeX(-d*10)

        if self.__hp < self.__MINHP:
            self.__hp = 0
            self.__healthBar = Bar(WIDTH, 20, 300, 22.5, "#FF0000", True)

        if self.__hp > self.__MAXHP:
            self.__hp = 30
            self.__healthBar = Bar(WIDTH-310, 20, 300, 22.5, "#FF0000", True)

    def pepperBoost(self):
        self.velocity += 10

    def tp(self, driver_active):
        if (driver_active):
            self.__x, self.__y = 200, 450
        else:
            self.__x, self.__y = 80, 190
        self.__rect.x, self.__rect.y = self.__x, self.__y

    def get_rect(self):
        return self.__rect

    def get_close_rect(self):
        return self.__rectClose

    def get_pos(self):
        return (self.__x, self.__y)

        #Hitbox debug

### ========================= CHEF ========================= ###

class Chef:

    def __init__(self, active, stamina = 300, color="", fileName=""):
        self.__stamina = stamina
        self.__active = active
        if active:
            bar_color = '#5841c7'
            filename = "stamina_bleu.png"
            bar_y = 20
        else:
            bar_color = '#66ba4e'
            filename = "stamina_verte.png"
            bar_y = 100

        self.__staminaBar = Bar(x=125, y=bar_y, width=stamina, height=22.5, color=bar_color)
        self.__staminaBarFrame = Box(x=75, y=bar_y-13, width=stamina, height=50, fileName=filename)
        self.__staminaBarFrame.scale(width=stamina+56, height=50)

    def getStamina(self):
        return self.__stamina

    def getStaminaBar(self):
        return self.__staminaBar

    def setStamina(self, stamina):
        self.__stamina = self.__staminaBar.setStamina(stamina)

    def changeStamina(self, x):
        self.__staminaBar.change_width(x)
        self.__stamina = self.__staminaBar.getWidth()
    def switch_state(self):
        self.__active = not(self.__active)

    def isActive(self):
        return self.__active

    def drawStamina(self):
        self.__staminaBar.draw()
        self.__staminaBarFrame.draw()

### ========================= ROAD ========================= ###

class Road():
    __time = None
    __item_codetable = None
    # Définition du niveau (pour la création de niveau)
    __level_def = None
    # Modèle concret du niveau (contenant des objets)
    __level_model = None
    __width = None
    __height = None
    __y = None
    __x = None
    __background = None
    __itemsize = None
    __nbcases = None

    def __init__(self, piste, game, time = 0, width=WIDTH, height=HEIGHT, itemsize=100, y=200):
        self.__time = time

        self.__piste = piste
        self.__game = game

        ##### Trucs qui concernent la musique et les timings ###
        self.__beat_time = perf_counter() # Initialisé réellement dans initial_tick(), qu'on appelle avant le premier tick()
        self.__bpm = self.__piste.get_bpm() * 2 # On double le BPM pour que la plus petite intervalle devienne un demi-temps
        self.__bps = self.__bpm/60
        # Seconds Per Beat = 1/(Beats per second)
        self.__spb = 1/self.__bps

        self.__beat_num = 1
        #####

        self.__width = width
        self.__height = height - 200
        self.__y = y
        # Position horizontale du premier segment de route
        self.__x = 0
        self.__itemsize = itemsize
        self.__rect = pygame.Rect(0, self.__y+30, self.__width, self.__height-60)

        self.__item_codetable = {
            11:Entity_type.CARROT,
            12:Entity_type.PEPPER
        }
        self.__level_def = self.__piste.get_data()

        ########## Précalculer les valeurs Y (hauteurs) des 3 lanes #########
        #
        border_width=30
        sep_width=10
        lane_width=(self.__height-(border_width*2)-(sep_width*2))/3

        entity_y = [0,0,0]
        entity_y[0] = (HEIGHT-self.__height + border_width + self.__height/3/2 - self.__itemsize/2)
        entity_y[1] = (HEIGHT-self.__height + self.__height/2 - self.__itemsize/2)
        entity_y[2] = (HEIGHT - border_width - self.__height/3/2 - self.__itemsize/2)

        ########## Générer le vrai modèle à partir de la définition #########

        self.__level_model=[]
        # Pour chaque lane (désignée par son indice, car on en a besoin
        for lane_num in range(len(self.__level_def)):
            lane_model=[]
            # La lane est une liste de délais entre les "notes"
            # Pour chaque délai, on va ajouter autant d'espaces que nécessaire, puis une note
            for delay in self.__level_def[lane_num]:
                # Tout délai inférieur à 1 devient 0, donc aucun espace, donc 2 "cases" consécutives.
                # Cela représente notre intervalle minimum, appelé plus tard un "beat"
                # Comme on a doublé le BPM, cela correspondra au final à un demi-temps
                delay=floor(delay)
                # Puis on ajoute autant d'espaces que de délai
                for _ in range(delay):
                    lane_model.append(0)
                # Et on termine par une entité
                # Sa position Y sera la valeur précalculée dans le tableau entity_y pour cette lane,
                # et son type est choisi au hasard
                lane_model.append(Entity(entity_y[lane_num], choice([t for t in Entity_type])))

            # La lane complète est ajoutée au modèle
            self.__level_model.append(lane_model)

        ######### Normaliser la longueur des 3 lanes #############
        # Calcul du nombre max de beats sur les 3 lanes
        self.__beat_count = max(len(self.__level_model[0]),len(self.__level_model[1]),len(self.__level_model[2]))
        # Pour chaque lane, ajouter autant de vides que nécessaire pour atteindre le max
        for lane in self.__level_model:
            for _ in range(self.__beat_count-len(lane)):
                lane.append(0)
        ##################################################################
        # print(self.__level_model)
        # print(len(self.__stage_model[0]))
        # print(len(self.__stage_model[1]))
        # print(len(self.__stage_model[2]))

        # Liste des items actifs (présents sur l'écran et en mouvement)
        self.__active_items = []

        self.__itemsize = itemsize
        self.__nbcases = ceil(self.__width/self.__itemsize)
        # print("Width=",self.__width," | size=",self.__itemsize," | nbcases : ",self.__nbcases)

        # Intervalle d'indices correspondant aux items actuellement visibles
        self.__visible_range = [0,-1]

        # Load background image
        self.__background = pygame.image.load(os.path.join('assets', 'road.png'))
        self.__background = pygame.transform.scale(self.__background, (self.__width, self.__height))

    def incr_time(self, t=1):
        self.__time += t

    def initial_tick(self):
        self.__beat_time = perf_counter()
        self.__beat_num = 0

    def tick(self):
        # On ne fait tout le tick QUE SI IL RESTE ENCORE DES BEATS
        # Sinon, c'est qu'on a gagné
        if self.__beat_num < self.__beat_count:

            # On récupère-stocke une fois le temps actuel
            # pour éviter les incohérences dues au temps d'exécution
            currtime = perf_counter()
            # Temps écoulé = temps actuel - temps précédent
            elapsed = currtime - self.__beat_time
            # print("elapsed = ",elapsed)
            # Si le temps écoulé est au moins celui d'un Beat,
            while elapsed >= self.__spb:
                # print("new beat : ",elapsed," elapsed")
                # Remise à zéro du temps écoulé
                self.__beat_time = currtime
                # Résolution du Beat
                # print("Beat ",self.__beat_num)

                # Pour les 3 lanes
                for lane in self.__level_model:
                    # Enlever et récupérer le 1er élément,
                    item = lane.pop(0)
                    # Si c'est une entité,
                    if item != 0:
                        # on l'ajoute à la liste des entités actives
                        self.__active_items.append(item)
                # Beat résolu !
                self.__beat_num += 1
                elapsed -= self.__spb

            # (i) A partir de là, les entités seront "lancées" avec
            # le bon écart de TEMPS entre elles.
            # Mais l'écart de DISTANCE sur l'écran va dépendre de la vitesse ;
            # Inversement, si on fixe la distance (ex : 1 beat = 60 pixels), ça détermine la vitesse.
            # Pour l'instant, on dit que 1 beat = taille d'un demi-item

            # Décalage = pixels par beat * beats par seconde * secondes écoulées
            # print("elapsed = ",elapsed)
            # shift_value = 10*self.__bps*elapsed
            shift_value = 20
            # print("Shift value = ",shift_value)

            # Décaler la route
            if self.__x < 0-self.__width:
                self.__x = 0
            else:
                self.__x -= shift_value
            # Décaler tous les items actifs
            for item in self.__active_items:
                item.shift(shift_value)
                # Si l'item se retrouve complètement hors de l'écran,
                # on l'enlève de la liste des items actifs
                if item.get_x() < 0-self.__itemsize:
                    self.__active_items.remove(item)
        else:
            # Plus aucun beat restant, c'est la victoire !
            self.__game.set_state(GameState.GAGNE)





        # # Indice premier élément visible
        # self.__visible_range[0] = max( (self.__time//self.__itemsize)-self.__nbcases, 0 )
        # # Indice dernier élément visible
        # self.__visible_range[1] = min((self.__time//self.__itemsize) + 1, len(self.__level_model[0]))
        #
        # # Pour tous les éléments visibles
        # for j in range(self.__visible_range[0], self.__visible_range[1]):
        #     # Calcul de la position X en fonction du temps et de l'indice
        #     posx = self.__width - ( self.__time-(j*self.__itemsize) )
        #     # print(j," : posx = ",posx)
        #     for lane_num in range(3):
        #         # Pour les 3 lanes, si la case d'indice J contient une Entity,
        #         # changer sa position X
        #         if self.__level_model[lane_num][j] != 0:
        #             self.__level_model[lane_num][j].move_x(posx)
        #         else:
        #             square = pygame.Rect(posx, (lane_num*self.__itemsize),self.__itemsize,self.__itemsize)
        #             pygame.draw.rect(WIN, pygame.Color(BLACK), square, 3)
        #
        # # Incrémenter le temps
        # self.__time+=12
        # # print("tick : ",self.__visible_range)

    def draw(self, debug):
        # Draw background
        # Since 1 background image fills the entire screen, we need to use two for scrolling :
        # - the first one is drawn on-screen and scrolls OUT to the left,
        WIN.blit(self.__background, (self.__x, self.__y))
        # - the second one is drawn off-screen and scrolls IN from the right
        WIN.blit(self.__background, (self.__width+self.__x, self.__y))

        # Draw les éléments
        for item in self.__active_items:
            item.draw(debug)




        # print("draw : ",self.__visible_range)

        # Pour tous les éléments visibles
        for j in range(self.__visible_range[0], self.__visible_range[1]):
            for lane_num in range(3):
                # Pour les 3 lanes, si la case d'indice J contient une Entity,
                # la dessiner
                if self.__level_model[lane_num][j] != 0:
                    self.__level_model[lane_num][j].draw(debug)

    def get_visible_items(self):
        # visible_items=[]
        # # Dans l'intervalle des éléments visibles
        # for i in range(self.__visible_range[0], self.__visible_range[1]):
        #     # Pour les 3 lanes
        #     for lane_num in range(3):
        #         # Si l'élément n'est pas une case vide, on l'ajoute à la liste
        #         if self.__level_model[lane_num][i] != 0:
        #             visible_items.append(self.__level_model[lane_num][i])
        #
        # return visible_items
        return self.__active_items


    def get_rect(self):
        return self.__rect
### ========================= ENUMS ========================= ###

class Entity_type(Enum):
    APPLE = "apple.png"
    BANANA = "banana.png"
    CARROT = "carrot.png"
    CORN = "corn.png"
    ORANGE = "orange.png"
    PEPPER = "pepper.png"
    PINEAPPLE = "pineapple.png"
    RASPBERRY = "raspberry.png"
    # TOMATO = "tomato.png"
    # ANANAS = "ananas.png"

### ========================= BUTTON ========================= ###

class Landscape:
    def __init__(self, filename="backgroundTest.png",time=0, speed=0, height=100):
        self.__height = height
        self.__time = time
        self.__bonusImage = pygame.image.load(os.path.join('assets', filename))
        self.__bonusImage = pygame.transform.scale(self.__bonusImage, (WIDTH, 300))
        self.__rect = pygame.Rect(0, 0, WIDTH, self.__height)
        self.__speed = speed

    def incr_time(self, t=1):
        self.__time += t

    def draw(self):
        # Draw background
        # Since 1 background image fills the entire screen, we need to use two for scrolling :
        # - the first one is drawn on-screen and scrolls OUT to the left,
        WIN.blit(self.__bonusImage, (0-(self.__time % WIDTH ) ,0))
        # - the second one is drawn off-screen and scrolls IN from the right
        WIN.blit(self.__bonusImage, (WIDTH -(self.__time % WIDTH) ,0))


class Button:
    "Create a button, then blit the surface in the while loop"

    def __init__(self, text, x=0, y=0, width=100, height=100, bg="#A87931", hover_color="#F5B85F"):
        self.__x = x
        self.__y = y
        self.__width = width
        self.__height = height
        self.__size = (self.__width, self.__height)
        self.__bg = bg
        self.__hover_color = hover_color
        self.action = None
        self.change_text(text, self.__bg)

    def change_text(self, text, bg="black"):
        self.__text = FONT.render(text, 1, pygame.Color("White"))
        self.__surface = pygame.Surface(self.__size)
        self.__surface.fill(bg)
        self.__surface.blit(self.__text, (self.__width/2-self.__text.get_width()/2, self.__height/2-self.__text.get_height()/2))
        self.__rect = pygame.Rect(self.__x, self.__y, self.__size[0], self.__size[1])

    def get_rect(self):
        return self.__rect

    def set_color(self, color):
        self.__surface.fill(color)

    def draw(self):

        x, y = pygame.mouse.get_pos()

        if self.__rect.collidepoint(x, y):
            rect = pygame.Rect(self.__x, self.__y, self.__width, self.__height)
            pygame.draw.rect(WIN, pygame.Color(self.__hover_color), rect, 5)

        WIN.blit(self.__surface, (self.__x, self.__y))

### ========================= STAMINABAR ========================= ###
class Bar:
    def __init__(self, x=0, y=0, width=300, height=50, color="#FF00FF", flip=False):
        self.__x = x
        self.__y = y
        self.__width = width
        self.__height = height
        self.__MIN = 0
        self.__MAX = 300
        self.__surface = pygame.Surface((self.__width, self.__height))
        self.__surface.set_alpha(0.5)
        self.__color = color
        self.__surface.fill(color)
        self.__rect = pygame.Rect(self.__x, self.__y, self.__width, self.__height)
        self.__updateSurface()

    def getWidth(self):
        return self.__width

    def change_width(self, change=1):
        if self.__MIN <= self.getWidth() <= self.__MAX:
            self.__width += change
            if self.__width < 0:
                self.__width = 0
            self.__updateSurface()

        if self.getWidth() < self.__MIN:
            self.setWidth(self.__MIN)
            self.__updateSurface()

        if self.getWidth() > self.__MAX:
            self.setWidth(self.__MAX)
            self.__updateSurface()

    def getheight(self):
        return self.__height

    def setWidth(self, width):
        self.__width = width
        self.__updateSurface()


    def setHeight(self, height):
        self.setHeight(height)

    def __updateSurface(self):
        self.__surface = pygame.Surface((self.__width, self.__height))
        self.__surface.fill(self.__color)
        self.__rect = pygame.Rect(self.__x, self.__y, self.__width, self.__height)

    def setColor(self, color):
        self.__color = color

    def setPos(self, x, y):
        self.__x = x
        self.__y = y

    def changeX(self, x):
        self.__x += x


    def draw(self):
        WIN.blit(self.__surface, (self.__x, self.__y))

class Box:
    def __init__(self, x = 0, y = 0, width = 75, height = 75, fileName=""):
        self.__surface = pygame.image.load(os.path.join('assets', fileName))
        self.__x = x
        self.__y = y


    def scale(self, width, height):
        self.__surface= pygame.transform.scale(self.__surface, (width, height))

    def draw(self):
        WIN.blit(self.__surface, (self.__x, self.__y))


### ========================= Entity ========================= ###
class Entity:
    def __init__(self, y,type=Entity_type.CARROT,velocity=1, fileName="", width=100, height=100):
        self.__type=type
        self.__bonusImage = pygame.image.load(os.path.join('assets', self.__type.value ))
        self.__bonusImage = pygame.transform.scale(self.__bonusImage, (100, 100))
        self.__x = WIDTH #comming from the right
        self.__y = y
        self.__width = width
        self.__height = height
        self.__rect = pygame.Rect(self.__x, self.__y, self.__width, self.__height)# -15 hitbox
        self.__velocity = velocity
        self.__sliced = False
        self.__missed = False

    def draw(self, debug):

        self.__rect.y = self.__y
        WIN.blit(self.__bonusImage, (self.__x, self.__y))
        if debug:
            pygame.draw.rect(WIN, WHITE, self.__rect, 5)
        # self.shift()

    def shift(self, distance=5):
        self.__x -= distance
        self.__rect.x = self.__x

    def move_x(self, x):
        self.__x=x
        self.__rect.x = self.__x

    def move_y(self, y):
        self.__y=y
        self.__rect.y = self.__y

    def get_x(self):
        return self.__x

    def get_rect(self):
        return self.__rect

    def slice(self):
        if not(self.__sliced):
            self.__sliced = True
            self.__bonusImage = pygame.image.load(os.path.join('assets', "sliced{}".format(self.__type.value) ))
            self.__bonusImage = pygame.transform.scale(self.__bonusImage, (100, 100))

    def miss(self):
        self.__missed = True

    def is_missed(self):
        return self.__missed

    def is_sliced(self):
        return self.__sliced

    def get_pos(self):
        return (self.__x, self.__y)

class Pepper(Entity):
    def __init__(self, velocity=1, y=0, width=60, height=60):
        super().__init__(velocity, "pepper.png", y, width, height)

class Carrot(Entity):
    def __init__(self, velocity=1,  y=0, width=60, height=60):
        super().__init__(velocity, "carrot.png", y, width, height)

class Orange(Entity):
    def __init__(self, velocity=1,  y=0, width=60, height=60):
        super().__init__(velocity, "orange.png", y, width, height)

class Raspberry(Entity):
    def __init__(self, velocity=1,  y=0, width=60, height=60):
        super().__init__(velocity, "raspberry.png", y, width, height)

class Pineapple(Entity):
    def __init__(self, velocity=1,  y=0, width=60, height=60):
        super().__init__(velocity, "pineapple.png", y, width, height)

class Apple(Entity):
    def __init__(self, velocity=1,  y=0, width=60, height=60):
        super().__init__(velocity, "apple.png", y, width, height)

class Corn(Entity):
    def __init__(self, velocity=1,  y=0, width=60, height=60):
        super().__init__(velocity, "corn.png", y, width, height)
### ========================= SETTINGS ========================= ###
class Settings:

    def __init__(self, sounds):
        self.__fx = sounds
        if not (os.path.isfile(os.path.join('settings.yaml'))):
            self.create_default(sounds)
        self.load()

    def get_keybinds(self):
        return self.__keybinds

    def create_default(self, sounds):
        pygame.mixer.music.set_volume(0.5)
        for key, sound in sounds.items():
            sound.set_volume(0.5)

        self.__keybinds = {'droite': 'd', 'gauche': 'q', 'bas': 's', 'haut': 'z', 'mode': 'left shift'}
        self.save()

    def load(self):
        with open('settings.yaml') as f:
            docs = yaml.load_all(f, Loader=yaml.FullLoader)

            for doc in docs:
                for k, v in doc.items():
                    if k == 'volume':
                        self.__volume = v
                    elif k == 'keybinds':
                        self.__keybinds = v

        pygame.mixer.music.set_volume(float("0.{}".format(self.__volume['music'])))
        for key, sound in self.__fx.items():
            sound.set_volume(float("0.{}".format(self.__volume['fx'])))

    def save(self):
        self.__volume = {'music': 0, 'fx': 0}
        self.__volume['music'] = int(round(pygame.mixer.music.get_volume(), 1)*10)
        self.__volume['fx'] = int(round(self.__fx['get'].get_volume(), 1)*10)

        data = {'volume': {}, 'keybinds': {}}
        data['volume'] = {'music': self.__volume['music'], 'fx': self.__volume['fx']}
        data['keybinds'] = self.__keybinds

        with open('settings.yaml', 'w') as f:
            data = yaml.dump(data, f)

class Score:
    def __init__(self):
        self.__score = 0
        self.__combo = 0
        self.__currentCombo = 0

        self.font = FONT
        self.__textCombo = "x{}".format(self.__score)
        self.__color = "#ffffff"
        self.__textComboSurface = self.font.render(self.__textCombo, True, self.__color)

        self.__textScore = " {}".format(self.__score)
        self.__textScoreSurface = self.font.render(self.__textScore, True, self.__color)

    def addCombo(self, x=1):
        self.__combo += x
        self.__currentCombo += x
        self.__textCombo = "x{}".format(self.__combo)
        self.__textComboSurface = self.font.render(self.__textCombo, True, self.__color)

    def addScore(self, x):
        self.__score += x
        self.__textScore = " {}".format(self.__score)
        self.__textScoreSurface = self.font.render(self.__textScore, True, self.__color)

    def drawCombo(self):
        WIN.blit(self.__textComboSurface, (WIDTH-self.__textComboSurface.get_width(), 100))

    def drawScore(self):
        WIN.blit(self.__textScoreSurface, (WIDTH-self.__textScoreSurface.get_width(), 150))

    def clearCombo(self):
        self.__combo = 0
        self.__currentCombo = 0
        self.__textCombo = "x{}".format(self.__combo)
        self.__textComboSurface = self.font.render(self.__textCombo, True, self.__color)

    def clearScore(self):
        sself.__score = 0
        self.__textScore = "x{}".format(self.__combo)
        self.__textScoreSurface = self.font.render(self.__textCombo, True, self.__color)

    def getCombo(self):
        return self.__combo

    def getScore(self):
        return self.__score

    def get_score(self):
        return self.__score

    def getCurrentCombo(self):
        return self.__currentCombo

class Piste:
    def __init__(self, num, musique, bpm, data, id_score):
        self.__num = num
        self.__musique = musique
        self.__data = data
        self.__bpm = bpm
        self.__id_score = id_score

    def get_data(self):
        return self.__data

    def get_musique(self):
        return self.__musique

    def get_id_score(self):
        return self.__id_score

    def get_bpm(self):
        return self.__bpm

class High_scores:
    def __init__(self):
        if os.path.isfile(os.path.join('scores.yaml')):
            self.load()
        else:
            self.create_default()

    def create_default(self):
        self.scores = {}
        for i in range(1,6):
            self.scores['piste{}'.format(i)] = {}
        self.save()

    def add_score(self, piste_id, nom, score):
        try:
            if self.scores[piste_id][nom] < score:
                self.scores[piste_id][nom] = score
        except:
            self.scores[piste_id][nom] = score

        # print(self.scores)

    def get_score(self, stage_num, position):
        scores = self.scores['piste{}'.format(stage_num)]
        scores = {k: v for k, v in sorted(scores.items(), key=lambda item: item[1])}
        try:
            name = list(scores)[-position]
            ret = (name, scores[name])
            return ret
        except IndexError:
            ret = (' ', 0)
            return ret

    def load(self):
        with open('scores.yaml') as f:
            docs = yaml.load_all(f, Loader=yaml.FullLoader)
            self.scores = {}
            for doc in docs:
                for k, v in doc.items():
                    self.scores[k] = v

    def save(self):
        data = self.scores

        with open('scores.yaml', 'w') as f:
            data = yaml.dump(data, f)


class InputBox:

    def __init__(self, x, y, w, h, text=''):
        self.rect = pygame.Rect(x, y, w, h)
        self.active_color = pygame.Color('firebrick')
        self.inactive_color = pygame.Color('lightcoral')
        self.color = self.inactive_color
        self.road = None
        self.font = FONT
        self.text = text
        self.txt_surface = self.font.render(text, True, self.color)
        self.active = False

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            # If the user clicked on the input_box rect.
            if self.rect.collidepoint(event.pos):
                # Toggle the active variable.
                self.active = True
                self.color = self.active_color
            else:
                self.active = False
                self.color = self.inactive_color
        if event.type == pygame.KEYDOWN:
            if self.active:
                if event.key == pygame.K_RETURN:
                    self.active = False
                    self.color = self.inactive_color
                #     print(self.text)
                #     self.text = ''
                if event.key == pygame.K_BACKSPACE:
                    self.text = self.text[:-1]
                else:
                    if event.key != pygame.K_RETURN:
                        self.text += event.unicode
                # Re-render the text.
                self.txt_surface = self.font.render(self.text, True, self.color)

    def update(self):
        # Resize the box if the text is too long.
        width = max(200, self.txt_surface.get_width()+10)
        self.rect.w = width

    def draw(self):
        # Blit the text.
        WIN.blit(self.txt_surface, (self.rect.x+5, self.rect.y + self.rect.height/2-self.txt_surface.get_height()/2))
        # Blit the rect.
        pygame.draw.rect(WIN, self.color, self.rect, 2)


class Game:

    def __init__(self, sounds):
        self.run = True
        self.last_switch = 0
        self.__show_hitboxes = False
        self.__state = GameState.TITLE_SCREEN

        self.driver = Chef(1, color="#4868E8")  # Actif
        self.slicer = Chef(0, color="#00FF00")

        self.driverPic = Box(x=10, y=10, width=50, height=50, fileName='bestHead.png')
        self.slicerPic = Box(x=0, y=70, width=50, height=50, fileName='etchHead.png')


        self.car = Car(rectRoad=pygame.Rect(0, 200+30, WIDTH, HEIGHT-60), velocity=10)
        self.score = Score()
        self.track_number = None
        self.__sounds = sounds

        self.__sprites = {}
        for type in Entity_type:
            self.__sprites[type] = (pygame.image.load(os.path.join('assets', type.value)))

    def set_last_switch(self, value):
        self.last_switch = value

    def get_last_switch(self):
        return self.last_switch

    def get_show_hitboxes(self):
        return self.__show_hitboxes

    def toggle_show_hitboxes(self):
        self.__show_hitboxes = not(self.__show_hitboxes)

    def get_sprite(self,sprite_type):
        return self.__sprites[sprite_type]

    def get_state(self):
        return self.__state

    def set_state(self, state):
        self.__state = state

    def set_road(self, road):
        self.road = road

    # Static
    def calcul_points(slicing_x, item_mid):
        distance = abs(slicing_x - item_mid)
        if distance == 0:
            points = 100
        else:
            points = min(100, 1 / (distance / 1000))
        # print(points)
        return int(points)

    def reset(self, num_piste=1):
        self.driver = Chef(1, color="#4868E8")  # Actif
        self.slicer = Chef(0, color="#00FF00")
        self.car = Car(rectRoad=pygame.Rect(0, 200+30, WIDTH, HEIGHT-60), velocity=10)
        self.score = Score()
        piste = Game.create_piste(num_piste)
        self.road = Road(piste,self)
        self.__sounds['truck'].play()
        pygame.mixer.music.load(os.path.join('..', 'music', piste.get_musique()))
        pygame.mixer.music.play()
        self.road.initial_tick()


    # Static
    def create_piste(numpiste):
        if numpiste == 1:
            return Piste(1, "stage1.ogg", 144, piste1_data, 'piste1')
        elif numpiste == 2:
            return Piste(2, "stage2.ogg", 98, piste2_data, 'piste2')


class GameState(Enum):
    TITLE_SCREEN = 0
    JEU = 1
    OPTIONS = 2
    CREDITS = 3
    PAUSE = 4
    PERDU = 5
    KEYBINDS = 6
    SELECTION = 7
    TUTO = 8
    HIGHSCORE = 9
    GAGNE = 10
