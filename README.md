# Gamejam2021

## L'équipe : Φ-Leap
Théo FONTANA, Alexandre LEGENDRE, Elian LORAUX, Eliott SAMMIER

## Le jeu : _Etch & Best - Road to Hellfest_

Etjerian et Bestov, deux amis, tiennent le food-truck Etch & Best, où ils servent hot-dogs, paninis et frites sur les places et parkings de Varsovie.
Ce mois-ci, comme tous les ans, ils se rendent au Hellfest pour aller voir leurs groupes de metal favoris en concert et profiter de l'affluence de clients.

Mais suite à une coïncidence spectaculaire et absolument inattendue pour les scénaristes, ils réalisent que tous les véhicules sur la route ont été
transformés en fruits et légumes géants ! Les voilà obligés de foncer parmi les végétaux, et impossible de s'arrêter la nuit sans prendre du retard !

Les deux comparses vont donc devoir se relayer pour dormir...

## Dépendances
Testé avec *Python 3.9* + bibliothèques Pygame et PyYAML (voir `requirements.txt` pour les versions). Le jeu fonctionnera peut-être avec des versions plus récentes, à vous d'essayer !

```sh
python -m venv venv
python -m pip install -r requirements.txt
```

## Lancement
```sh
cd src
python main.py
```

